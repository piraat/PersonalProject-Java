package wordCount;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String filePath = args[0]; 
		if(filePath == null) {
			System.out.println("please input the file name!");
			System.exit(0);
		}
//		String filePath = "E:\\eclipse-workspace\\WordCount\\t9.txt";
		String resultPath = "result.txt";
		String[] strs = FileIO.readFile(filePath);
		if(strs == null) {
			System.out.println("couldn't find the file");
			System.exit(0);
		}
		String str = FileIO.readFileByStream(filePath);
		ArrayList<String> words = WordCount.countWords(strs);
		System.out.println("characters: "+ WordCount.countChar(str));
		FileIO.writeFile(resultPath, "characters: "+ WordCount.countChar(str));
		FileIO.writeFile(resultPath, "words: "+ words.size());
		FileIO.writeFile(resultPath, "lines: "+ WordCount.countLines(strs));
		System.out.println("words: "+ words.size());
		System.out.println("lines: "+ WordCount.countLines(strs));
		for(int i = 0 ; i < 10 ; i++ ) {
			if(i == words.size()) {
				break;
			}
			FileIO.writeFile(resultPath, words.get(i));
			System.out.println(words.get(i));
		}
	}

}
