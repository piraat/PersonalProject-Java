package wordCount;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FileIO {

	
	
	public static String readFileByStream(String filePath)throws IOException{
        FileReader reader = new FileReader(filePath);
        File file = new File(filePath);
    	long i = file.length() ;
        char[] c = new char[(int) i];  
        reader.read(c);
        String str = String.valueOf(c);
        reader.close();
        return str;
	}
	
	public static String[] readFile(String filePath)throws IOException{
        BufferedReader reader = null;
    	int i = 0 ;
        String[] str = new String[120000];
        File file = new File(filePath);
        if(!file.exists()) {
        	return null;
        }
        try {  
            reader = new BufferedReader(new FileReader(filePath));
            String tempString ;
            while ((tempString = reader.readLine()) != null) {   
            		str[i++] = tempString;
            }  
            reader.close();  
            return str;
        } catch (IOException e) {  
            e.printStackTrace();  
        	return null;
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
	}
	

	
	public static String writeFile(String filePath, String str) throws IOException {
		BufferedWriter out = null;     
        try {     
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath, true)));  
            out.write(str);    
            out.newLine();    
        } catch (Exception e) {     
            e.printStackTrace();  
        } finally {     
            try {     
                if(out != null){  
                    out.close();     
                }  
            } catch (IOException e) {     
                e.printStackTrace();     
            }     
        }     
		return "";
	}

}
